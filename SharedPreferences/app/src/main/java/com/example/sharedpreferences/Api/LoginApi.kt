package com.example.sharedpreferences.Api

import com.example.sharedpreferences.model.LogInRequest
import com.example.sharedpreferences.model.LoginModel
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface LoginApi {
    @Headers("Content-Type: application/json")
    @POST("user/login?_format=json")

    fun getToken(@Body logInRequest: LogInRequest): Observable<LoginModel>
    companion object Factory{
        fun create():LoginApi{
            val retrofit= Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://edugaonlabs.com/")
                .build()

            return (retrofit.create(LoginApi::class.java))
        }
    }
}