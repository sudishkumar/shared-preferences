package com.example.sharedpreferences.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class LoginModel(
    @SerializedName("current_user")
    val currentUser :CurrentUser,

    @SerializedName("csrf_token")
    val csrfToken :String? = null,

    @SerializedName("logout_token")
    val logoutToken :String? = null
)
data class CurrentUser(
    @SerializedName("uid")
    val uid :String? = null,

    @SerializedName("roles")
    val roles : List<String>
    )
data class LogInRequest(
    @Expose
    @SerializedName("name")
    val userName: String? = null,
    @Expose
    @SerializedName("pass")
    val password:String? = null,
)