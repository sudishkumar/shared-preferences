package com.example.sharedpreferences.activity

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.sharedpreferences.Api.UserApi
import com.example.sharedpreferences.R
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        tokenTest()

        }

    fun tokenTest(){

        var tokenPreferences = this.getPreferences(Context.MODE_PRIVATE)
        val tokenPreferencesTest=tokenPreferences.getString("save_token","no_token")

        if (tokenPreferencesTest!="no_token"){
            startActivity(Intent(this,LoginActivity::class.java))
            finish()

        }else{
           loadApi()
        }
    }

    fun loadApi(){
        val ApiService= UserApi.create()

        ApiService.getToken()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({result->
                saveToken(result.userEntryToken)
            },{ error->
                error.printStackTrace()
            })
         }

    private fun saveToken(userEntryToken: String?) {
        val savePreference = this.getPreferences(Context.MODE_PRIVATE)
        with(savePreference.edit()){
            putString("save_token", userEntryToken)
                .commit()
        }
    }
}
