package com.example.sharedpreferences.activity

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.sharedpreferences.Api.LoginApi
import com.example.sharedpreferences.R
import com.example.sharedpreferences.model.CurrentUser
import com.example.sharedpreferences.model.LogInRequest
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class LoginActivity : AppCompatActivity() {
    lateinit var name :EditText
    lateinit var password:EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val sigin = findViewById<Button>(R.id.login_button)
         name=findViewById(R.id.name_editText)
         password=findViewById(R.id.password_editText)

        sigin.setOnClickListener {
            callLoginApi()
        }
    }
    fun callLoginApi(){
        val name=name.text.toString().trim()
        val pass=password.text.toString().trim()
        val logInRequest= LogInRequest(name,pass)
        val ApiService= LoginApi.create()

        ApiService.getToken(logInRequest)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({result->
               result.csrfToken
                val intent = Intent(this,HomeActivity::class.java)
                startActivity(intent)
                finish()

            },
                { error->
                //error.printStackTrace()
                Toast.makeText(this,"usr name and password invalid",Toast.LENGTH_LONG).show()
                }
            )
    }
}